import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { DestinoViajes } from 'src/app/models/destino-viajes.models';
import { AppState } from 'src/app/app.module';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.models';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViajes>;
  updates:string[];
  all;

  constructor(
      public destinosApiClient:DestinosApiClient,
      public store: Store<AppState>
    ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit() {
    this.store.select(state => state.destinos)
      .subscribe(data => {
        let d = data.favorito;
        if (d != null) {
          this.updates.push("Se eligió: " + d.nombre);
        }
      });
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  agregado(d:DestinoViajes) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d:DestinoViajes) {
    this.destinosApiClient.elegir(d);
  }

  getAll() {

  }
}